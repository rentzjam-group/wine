/*
 * Copyright 2021 Jimmy Rentz
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(winlogon);

int __cdecl wmain(int argc, WCHAR *argv[])
{
    /* Only allow one instance.
    Also, let wineserver cleanup the event.*/
    HANDLE running_evt = CreateEventW(NULL, TRUE, FALSE, L"Global\\WinLogon_Running");

    if (running_evt != NULL && GetLastError() == ERROR_ALREADY_EXISTS) return 0;

    WINE_FIXME("stub\n");

    /* Just suspend so it does nothing.*/
    SuspendThread(GetCurrentThread());

    return 0;
}
